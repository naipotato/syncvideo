/* SyncVideo
 * Copyright (C) 2019 Nahuel Gomez Castro <nahual_gomca@outlook.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

class SyncVideo.App : Gtk.Application {

    public App () {
        Object (
            application_id: "com.gitlab.nahuelwexd.SyncVideo",
            flags: ApplicationFlags.FLAGS_NONE
        );
    }

    public static int main (string[] args) {
        return new SyncVideo.App ().run (args);
    }

    protected override void activate () {
        var window = this.active_window;

        if (window == null) {
            window = new SyncVideo.MainWindow (this);
            window.show_all ();
        }

        window.present ();
    }
}
