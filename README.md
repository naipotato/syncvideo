SyncVideo
=========

**THIS IS WORK IN PROGRESS**, is not yet ready for use.

Overview
--------

SyncVideo is a new project that I started in Vala and GTK + 3.0. This project will be a video player that will allow
simultaneous viewing of videos with another person. You will be able to play videos, pause them, move forward and
backward. As an addition, you can activate an option to decide if the application evaluates the checksum or not, in
order to verify if both are reproducing exactly the same file.

Current Status
--------------

The project has a short life span, so at the moment there is no way that the code published here will be able to perform
any of the above mentioned characteristics. I'll be working on it during my free time, so if you want to move faster,
you're free to collaborate 😀️

Build & Install
---------------

In order to build this project, you will need the following dependencies in your system:

- `meson`
- `vala`
- `gtk-3.0`

Then, clone, build and install this project by executing the following commands:

```sh
git clone https://gitlab.com/nahuelwexd/syncvideo.git SyncVideo
cd SyncVideo
meson setup --buildtype release --prefix /usr build .
ninja -C build install
```

If you ran into any issue, feel free to open a new issue.

Contributing
------------

You want to contribute with SyncVideo? Yay!

In order to contribute to this project, you will need a code editor. Use the one that you know and love, there is no
need to use a specific one. You want an IDE? Then GNOME Builder will fit perfectly for this project. For Visual Studio
Code users, there is a `.vscode` folder with basic settings to build and launch this project. Also I putted an
`.editorconfig` file, with format settings for any code editor or IDE that supports it.

The code rules that I am following for this project is: 1TBS with mandatory braces, 120 character limit per lime, and
emojis for commits (check [this](https://gitmoji.carloscuesta.me/)).

Feel free to open a merge request at any time!

License
-------

SyncVideo is licensed under the [GNU General Public License v3](LICENSE).

**[tl;dr](https://www.tldrlegal.com/l/gpl-3.0)**: You may copy, distribute and modify this program as long as you track
changes/dates in source files. Any modifications to or software including (via compiler) GPL-licensed code must also be
made available under the GPL along with build & install instructions.
